export class Announcement {
  public announcementId: number;
  public appUserId: number;
  public gameId: number;
  public announcementTitle: string;
  public announcementDescription: string;
  public announcementCreatedTime: Date;
  public appGroupId: number;

  constructor() {
    this.announcementId = null;
    this.appUserId = null;
    this.gameId = null;
    this.announcementTitle = null;
    this.announcementDescription = null;
    this.announcementCreatedTime = null;
    this.appGroupId = null;
  }
}
