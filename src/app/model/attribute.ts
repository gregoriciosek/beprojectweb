export class Attribute {
  public attributeId: number;
  public gameId: number;
  public attributeName: string;
  public attributeValue: number;

  constructor() {
    this.attributeId = null;
    this.gameId = null;
    this.attributeName = null;
    this.attributeValue = null;
  }
}
