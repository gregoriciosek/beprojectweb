import {AppGroup} from "./appgroup";

export interface DialogDataAppGroup {
  appGroup: AppGroup;
}
