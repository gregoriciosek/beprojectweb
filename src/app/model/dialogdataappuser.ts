import {AppUser} from "./appuser";

export interface DialogDataAppUser {
  appUser: AppUser;
}
