export class AppGroupPlayer {
  public appGroupPlayerId: number;
  public appGroupId: number;
  public appUserId: number;

  constructor() {
    this.appGroupPlayerId = null;
    this.appGroupId = null;
    this.appUserId = null;
  }
}
