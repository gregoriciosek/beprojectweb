export class AppUser {
    public appUserId: number;
    public appUserTypeId: number;
    public appUserUsername: string;
    public appUserPassword: string;
    public appUserEmail: string;
    public appUserNickname: string;
    public appUserDateOfBirth: Date;
    public createdAt: Date;
    public appUserDiscord: string;
    public appUserTeamspeak: string;
    public appUserGender: string;
    public appUserProfileImgUrl: string;

    constructor() {
        this.appUserId = null;
        this.appUserTypeId = null;
        this.appUserUsername = null;
        this.appUserPassword = null;
        this.appUserEmail = null;
        this.appUserNickname = null;
        this.appUserDateOfBirth = null;
        this.createdAt = null;
        this.appUserDiscord = null;
        this.appUserTeamspeak = null;
        this.appUserGender = null;
        this.appUserProfileImgUrl = null;
    }
}
