export class AppGroup {
  public appGroupId: number;
  public appGroupName: string;
  public appGroupDescription: string;
  public gameId: number;
  public appUserId: number;

  constructor() {
    this.appGroupId = null;
    this.appGroupName = null;
    this.appGroupDescription = null;
    this.gameId = null;
    this.appUserId = null;
  }
}
