export class ChatUsersAndChatMessage {
  public appUserId: number;
  public appAppUserId: number;
  public chatMessageMsg: string;

  constructor() {
    this.appUserId = null;
    this.appAppUserId = null;
    this.chatMessageMsg = null;
  }

}
