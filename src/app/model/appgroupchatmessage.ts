export class AppGroupChatMessage {
  public appGroupChatMessageId: number;
  public appGroupChatId: number;
  public appUserId: number;
  public appGroupChatMessageMsg: string;
  public appGroupChatMessageDate: Date;

  constructor() {
    this.appGroupChatMessageId = null;
    this.appGroupChatId = null;
    this.appUserId = null;
    this.appGroupChatMessageMsg = null;
    this.appGroupChatMessageDate = null;
  }
}
