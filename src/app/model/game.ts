export class Game {
  public gameId: number;
  public gameName: string;
  public gameDescription: string;
  public gameAttributeTitle: string;
  public gameImageUrl: string;

  constructor() {
  this.gameId = null;
  this.gameName = null;
  this.gameDescription = null;
  this.gameAttributeTitle = null;
  this.gameImageUrl = null;
  }
}
