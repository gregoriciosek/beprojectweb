export class AppUserGame {
  public appUserGameId: number;
  public appUserId: number;
  public gameId: number;
  public attributeId: number;

  constructor() {
    this.appUserGameId = null;
    this.appUserId = null;
    this.gameId = null;
    this.attributeId = null;
  }

}
