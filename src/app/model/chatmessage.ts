export class ChatMessage {
  public chatMessageId: number;
  public appUserId: number;
  public chatId: number;
  public chatMessageMsg: string;
  public chatMessageDate: Date;
  constructor() {
    this.chatMessageId = null;
    this.appUserId = null;
    this.chatId = null;
    this.chatMessageMsg = null;
    this.chatMessageDate = null;
  }
}
