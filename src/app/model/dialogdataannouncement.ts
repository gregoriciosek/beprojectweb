import {Announcement} from "./announcement";
import {AppGroup} from "./appgroup";

export interface DialogDataAnnouncement {
  announcement: Announcement;
  appGroup: AppGroup;
}
