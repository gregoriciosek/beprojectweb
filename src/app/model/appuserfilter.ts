export class AppUserFilter {

  public gameAttribute: string;
  public ageFrom: number;
  public ageTo: number;
  public gender: string;
  constructor() {
    this.gameAttribute = null;
    this.ageFrom = null;
    this.ageTo = null;
    this.gender = null;
  }
}
