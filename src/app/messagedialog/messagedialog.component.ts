import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from "../model/appuser";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DialogDataAppUser} from "../model/dialogdataappuser";
import {Chat} from "../model/chat";
import {Chatservice} from "../service/chatservice";
import {ChatUsersAndChatMessage} from "../model/chatusersandchatmessage";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {NotificationService} from "../service/notification.service";

@Component({
  selector: 'app-messagedialog',
  templateUrl: './messagedialog.component.html',
  styleUrl: './messagedialog.component.css'
})
export class MessagedialogComponent implements OnInit, OnDestroy {

  public appUserToDialog: AppUser = new AppUser();
  public message : string = '';

  constructor(
    public chatService: Chatservice,
    public authenticationService: AuthenticationService,
    public dialogRef: MatDialogRef<MessagedialogComponent>,
    public notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataAppUser,
  ) {
  }

  ngOnInit(): void {
    this.appUserToDialog = this.data.appUser;
    console.log(this.appUserToDialog);
  }

  ngOnDestroy(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendMessage() {
    const chatUsersAndChatMessage: ChatUsersAndChatMessage = new ChatUsersAndChatMessage();
    chatUsersAndChatMessage.appUserId = this.authenticationService.getAppUserFromLocalCache().appUserId;
    chatUsersAndChatMessage.appAppUserId = this.appUserToDialog.appUserId;
    chatUsersAndChatMessage.chatMessageMsg = this.message;
    this.chatService.addOrUpdateChatWithMessage(chatUsersAndChatMessage).subscribe(
      (response: Chat) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Wysłano wiadomość do " + this.appUserToDialog.appUserUsername);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }
}
