import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppuserComponent } from './appuser/appuser.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationModule } from './notification.module';
import { NotificationService } from './service/notification.service';
import { AuthenticationService } from './service/authentication.service';
import { AppuserService } from './service/appuser.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card'
import {MatButtonModule} from '@angular/material/button'
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import { AppGroupComponent } from './appgroup/appgroup.component';
import { ChatComponent } from './chat/chat.component';
import { GameComponent } from './game/game.component';
import { ProfileComponent } from './profile/profile.component';
import { AnnouncementComponent } from './announcement/announcement.component';
import {MatListModule} from "@angular/material/list";
import {AppuserdialogComponent} from "./appuserdialog/appuserdialog.component";
import {MatDialogActions, MatDialogContent} from "@angular/material/dialog";
import {AppgroupdialogComponent} from "./appgroupdialog/appgroupdialog.component";
import {AppgroupeditdialogComponent} from "./appgroupeditdialog/appgroupeditdialog.component";
import {AnnouncedialogComponent} from "./announcedialog/announcedialog.component";
import {AnnounceeditdialogComponent} from "./announceeditdialog/announceeditdialog.component";
import {MessagedialogComponent} from "./messagedialog/messagedialog.component";
import {AppGroupCreateDialogComponent} from "./appgroupcreatedialog/appgroupcreatedialog.component";
import {AnnouncecreatedialogComponent} from "./announcecreatedialog/announcecreatedialog.component";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AppuserComponent,
    HomeComponent,
    AppGroupComponent,
    ChatComponent,
    GameComponent,
    ProfileComponent,
    AnnouncementComponent,
    AppuserdialogComponent,
    AppgroupdialogComponent,
    AppgroupeditdialogComponent,
    AnnouncedialogComponent,
    AnnounceeditdialogComponent,
    MessagedialogComponent,
    AppGroupCreateDialogComponent,
    AnnouncecreatedialogComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        NotificationModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatGridListModule,
        MatListModule,
        MatDialogActions,
        MatDialogContent,
        MatIconModule
    ],
  providers: [NotificationService, AuthenticationService, AppuserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
