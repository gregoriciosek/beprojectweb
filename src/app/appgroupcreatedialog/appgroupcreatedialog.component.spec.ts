import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppGroupCreateDialogComponent } from './appgroupcreatedialog.component';

describe('AppGroupCreateDialogComponent', () => {
  let component: AppGroupCreateDialogComponent;
  let fixture: ComponentFixture<AppGroupCreateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppGroupCreateDialogComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppGroupCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
