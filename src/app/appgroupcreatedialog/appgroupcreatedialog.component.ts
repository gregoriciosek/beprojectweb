import {Component, OnDestroy, OnInit} from '@angular/core';
import {Game} from "../model/game";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {GenderEnum} from "../model/genderEnum";
import {AppUser} from "../model/appuser";
import {AppGroupService} from "../service/appgroup.service";
import {AuthenticationService} from "../service/authentication.service";
import {AppGroup} from "../model/appgroup";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {NotificationService} from "../service/notification.service";

@Component({
  selector: 'app-appgroupcreatedialog',
  templateUrl: './appgroupcreatedialog.component.html',
  styleUrl: './appgroupcreatedialog.component.css'
})
export class AppGroupCreateDialogComponent implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public activeGame: Game = new Game();
  public appGroupNewName: string ='';
  public appGroupNewDescription: string ='';
  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public dialogRef: MatDialogRef<AppuserdialogComponent>
  ) {  }

  ngOnInit(): void {
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
  }

  ngOnDestroy(): void {
  }

  createNewGroup(){
    const appGroup: AppGroup = new AppGroup();
    appGroup.appGroupName = this.appGroupNewName;
    appGroup.appGroupDescription = this.appGroupNewDescription;
    appGroup.appUserId = this.appUser.appUserId;
    appGroup.gameId = this.activeGame.gameId;
    this.appGroupService.createAppGroup(appGroup).subscribe(
      (response: AppGroup) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Utworzono grupę " + appGroup.appGroupName);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  protected readonly GenderEnum = GenderEnum;
}
