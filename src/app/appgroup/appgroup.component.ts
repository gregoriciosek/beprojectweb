import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppUser} from "../model/appuser";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../service/authentication.service";
import {GameService} from "../service/game.service";
import {HttpErrorResponse} from "@angular/common/http";
import {AppGroup} from "../model/appgroup";
import {AppgroupdialogComponent} from "../appgroupdialog/appgroupdialog.component";
import {AppGroupService} from "../service/appgroup.service";
import {AppGroupCreateDialogComponent} from "../appgroupcreatedialog/appgroupcreatedialog.component";
import {NotificationType} from "../enum/notificationtype.enum";
import {NotificationService} from "../service/notification.service";
import {AppgroupeditdialogComponent} from "../appgroupeditdialog/appgroupeditdialog.component";

@Component({
  selector: 'app-appgroup',
  templateUrl: './appgroup.component.html',
  styleUrl: './appgroup.component.css'
})
export class AppGroupComponent implements OnInit, OnDestroy{

  public appUser: AppUser;
  public appGroups: AppGroup[] = new Array<AppGroup>();
  public actualGame: string = '';

  constructor(public dialog: MatDialog,
              private router: Router,
              private authenticationService: AuthenticationService,
              private appGroupService: AppGroupService,
              private notificationService: NotificationService,
              private gameService: GameService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.actualGame = this.gameService.getGameFromLocalCache().gameName;
      this.getGroups();
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {
  }

  openDialog(appGroupId : number): void {
    const appGroupToDialog : AppGroup = this.appGroups.find(i => i.appGroupId === appGroupId);
    const dialogRef = this.dialog.open(AppgroupdialogComponent, {
      width: "1200px", height: "700px", data: {appGroup: appGroupToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialogCreateGroup(): void {
    const dialogRef = this.dialog.open(AppGroupCreateDialogComponent, {
      width: "600px", height: "440px"
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getGroups();
    });
  }

  openDialogEditGroup(appGroupId : number): void {
    const appGroupToDialog : AppGroup = this.appGroups.find(i => i.appGroupId === appGroupId);
    const dialogRef = this.dialog.open(AppgroupeditdialogComponent, {
      width: "600px", height: "440px", data: {appGroup: appGroupToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getGroups();
    });
  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }
  public goToHome() {
    this.router.navigateByUrl('/home');
  }
  public goToChat() {
    this.router.navigateByUrl('/chat');
  }
  public goToProfile() {
    this.router.navigateByUrl('/profile');
  }

  public goToGroupDetails(appGroupId : number) {
    this.openDialog(appGroupId);
  }

  public getGroups() {
    this.appGroupService.getAppGroupsForAppUser(this.appUser.appUserId, this.gameService.getGameFromLocalCache().gameId).subscribe(
      (response: AppGroup[]) => {
        this.appGroups = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  deleteGroup(appGroup: AppGroup){
    this.appGroupService.deleteAppGroup(appGroup.appGroupId).subscribe(
      (response: any) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Usunięto grupę " + appGroup.appGroupName);
        this.getGroups();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

}

