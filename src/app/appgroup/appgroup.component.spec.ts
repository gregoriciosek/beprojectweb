import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppGroupComponent } from './appgroup.component';

describe('AppgroupComponent', () => {
  let component: AppGroupComponent;
  let fixture: ComponentFixture<AppGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppGroupComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
