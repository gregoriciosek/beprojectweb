import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { AppUser } from '../model/appuser';
import { Subscription } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { NotificationService } from '../service/notification.service';
import { NotificationType } from '../enum/notificationtype.enum';
import { FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from '../utils/my-error-state-matcher';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit, OnDestroy{

  public emailFormControl = new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);
  public matcher = new MyErrorStateMatcher();

  public showLoading: boolean = false;
  private subscription: Subscription[] = [];

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.router.navigateByUrl('/game')
    }
  }

  public onRegister(appUser: AppUser): void {
    this.showLoading = true;
    const appUserTarget: AppUser = appUser;
    appUserTarget.appUserEmail = this.emailFormControl.getRawValue();
    console.log(appUserTarget);
    this.subscription.push(
      this.authenticationService.register(appUserTarget).subscribe(
        (response: HttpResponse<AppUser>) => {
          this.showLoading = false;
          this.notificationService.notify(NotificationType.INFO, "Konto zostało utworzone pomyślnie.");
          this.router.navigateByUrl('/login');
        }, (error: HttpErrorResponse) => {
          this.showLoading = false;
          this.notificationService.notify(NotificationType.ERROR, error.message);
        })
      );
  }

  ngOnDestroy(): void {

  }

}

