import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { AppUser } from '../model/appuser';
import {GameService} from "../service/game.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit, OnDestroy{

  public appUser: AppUser;
  public actualGame: string = '';

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
              private gameService: GameService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.actualGame = this.gameService.getGameFromLocalCache().gameName;
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {

  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }
  public goToChat() {
    this.router.navigateByUrl('/chat');
  }

  public goToAnnouncement() {
    this.router.navigateByUrl('/announcement');
  }
  public goToGames() {
    this.router.navigateByUrl('/game');
  }
  public goToProfile() {
    this.router.navigateByUrl('/profile');
  }
  public goToPlayers() {
    this.router.navigateByUrl('/player');
  }
  public goToGroups() {
    this.router.navigateByUrl('/group');
  }

}
