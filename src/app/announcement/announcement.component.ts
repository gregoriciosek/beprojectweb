import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppUser} from "../model/appuser";
import {AppGroup} from "../model/appgroup";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../service/authentication.service";
import {AppGroupService} from "../service/appgroup.service";
import {NotificationService} from "../service/notification.service";
import {GameService} from "../service/game.service";
import {AppgroupdialogComponent} from "../appgroupdialog/appgroupdialog.component";
import {AppGroupCreateDialogComponent} from "../appgroupcreatedialog/appgroupcreatedialog.component";
import {AppgroupeditdialogComponent} from "../appgroupeditdialog/appgroupeditdialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {Announcement} from "../model/announcement";
import {AnnouncementService} from "../service/announcement.service";
import {NotificationType} from "../enum/notificationtype.enum";
import {AnnouncecreatedialogComponent} from "../announcecreatedialog/announcecreatedialog.component";
import {AnnounceeditdialogComponent} from "../announceeditdialog/announceeditdialog.component";
import {AnnouncedialogComponent} from "../announcedialog/announcedialog.component";

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrl: './announcement.component.css'
})
export class AnnouncementComponent implements OnInit, OnDestroy{

  public appUser: AppUser;
  public appGroups: AppGroup[] = new Array<AppGroup>();
  public announcements: Announcement[] = new Array<Announcement>();
  public actualGame: string = '';

  constructor(public dialog: MatDialog,
              private router: Router,
              private authenticationService: AuthenticationService,
              private appGroupService: AppGroupService,
              private notificationService: NotificationService,
              private announcementService: AnnouncementService,
              private gameService: GameService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.actualGame = this.gameService.getGameFromLocalCache().gameName;
      this.getGroups();
      this.getAnnouncements();
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {
  }

  openDialog(annnouncementId : number): void {
    const announcementToDialog : Announcement = this.announcements.find(value => value.announcementId === annnouncementId);
    const appGroupToDialog : AppGroup = this.appGroups.find(i => i.appGroupId === announcementToDialog.appGroupId);

    const dialogRef = this.dialog.open(AnnouncedialogComponent, {
      width: "1100px", height: "480px", data: {announcement: announcementToDialog,appGroup: appGroupToDialog}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialogCreateAnnouncement(): void {
    const dialogRef = this.dialog.open(AnnouncecreatedialogComponent, {
      width: "600px", height: "540px"
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAnnouncements();
    });
  }

  openDialogEditAnnouncement(announcementId : number): void {
    const announcement : Announcement = this.announcements.find(i => i.announcementId === announcementId);
    const appGroup : AppGroup = this.appGroups.find(value => value.appGroupId === announcement.appGroupId);
    const dialogRef = this.dialog.open(AnnounceeditdialogComponent, {
      width: "600px", height: "540px", data: {announcement: announcement, appGroup: appGroup}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAnnouncements();
    });
  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }
  public goToHome() {
    this.router.navigateByUrl('/home');
  }
  public goToChat() {
    this.router.navigateByUrl('/chat');
  }
  public goToProfile() {
    this.router.navigateByUrl('/profile');
  }

  public goToAnnouncementDetails(annoucenemtId : number) {
    this.openDialog(annoucenemtId);
  }

  public getAppGroupName(appGroupId: number) {
    return this.appGroups.find(value => value.appGroupId === appGroupId).appGroupName;
  }

  public getGroups() {
    this.appGroupService.getAppGroupsForGameId(this.gameService.getGameFromLocalCache().gameId).subscribe(
      (response: AppGroup[]) => {
        this.appGroups = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public getAnnouncements() {
    this.announcementService.getAnnouncementsByGameId(this.gameService.getGameFromLocalCache().gameId).subscribe(
      (response: Announcement[]) => {
        this.announcements = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  deleteAnnouncement(announcement: Announcement){
    this.announcementService.deleteAnnouncement(announcement).subscribe(
      (response: any) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Usunięto ogłoszenie " + announcement.announcementTitle);
        this.getAnnouncements();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

}
