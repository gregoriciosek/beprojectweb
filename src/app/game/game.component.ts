import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {AppUser} from "../model/appuser";
import {AuthenticationService} from "../service/authentication.service";
import {GenderEnum} from "../model/genderEnum";
import {FormControl} from "@angular/forms";
import {Game} from "../model/game";
import {GameService} from "../service/game.service";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {Attribute} from "../model/attribute";
import {AttributeService} from "../service/attribute.service";
import {AppUserGame} from "../model/appusergame";
import {NotificationType} from "../enum/notificationtype.enum";
import {NotificationService} from "../service/notification.service";
import {MatSelect} from "@angular/material/select";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.css'
})
export class GameComponent implements OnInit, OnDestroy{
  @ViewChild(MatSelect) matSelect: MatSelect;

  public appUser: AppUser = new AppUser();
  public games: Game[] = new Array<Game>();
  public attributesForGameOne: Attribute[] = new Array<Attribute>();
  public attributesForGameTwo: Attribute[] = new Array<Attribute>();
  public attributesForGameThree: Attribute[] = new Array<Attribute>();
  public gameOneAttributeValue: number = 0;
  public gameTwoAttributeValue: number = 0;
  public gameThreeAttributeValue: number = 0;
  public gameOneName: string = '';
  public gameOneDescription: string = '';
  public gameOneAttributeName: string ='';
  public gameTwoName: string = '';
  public gameTwoDescription: string = '';
  public gameTwoAttributeName: string ='';
  public gameThreeName: string = '';
  public gameThreeDescription: string = '';
  public gameThreeAttributeName: string ='';
  public gameOneAttributeFormControl = new FormControl();
  public gameTwoAttributeFormControl  = new FormControl();
  public gameThreeAttributeFormControl  = new FormControl();

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private gameService: GameService,
              private attributeService: AttributeService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
        this.appUser = this.authenticationService.getAppUserFromLocalCache();
        this.loadGames();
        this.loadAttributesForGameOne();
        this.loadAttributesForGameTwo();
        this.loadAttributesForGameThree();
        this.loadAttributesForGameOneAndAppUser();
        this.loadAttributesForGameTwoAndAppUser();
        this.loadAttributesForGameThreeAndAppUser();
        this.gameService.removeGameFromLocalCache();
    } else {
      this.router.navigateByUrl('/login');
    }

  }
  ngOnDestroy(): void {
  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }

  public setGame(gameId: number, attributes: Attribute[], attributeId: number) {
    this.gameService.addGameToLocalCache(this.games[gameId]);
    this.router.navigateByUrl('/home');
  }

  public setGameOneAttribute() {
    const appUserGame = new AppUserGame();
    appUserGame.gameId = 1;
    appUserGame.appUserId = this.appUser.appUserId;
    appUserGame.attributeId = this.gameOneAttributeFormControl.value;
    this.gameService.addOrUpdateAttributeForGameAndUser(appUserGame).subscribe(
      (response: AppUserGame) => {
        this.notificationService.notify(NotificationType.INFO, "Atrybut został zmieniony");
      }, (error: HttpErrorResponse) => {
        this.notificationService.notify(NotificationType.ERROR, error.message);
      });
  }

  public setGameTwoAttribute() {
    const appUserGame = new AppUserGame();
    appUserGame.gameId = 2;
    appUserGame.appUserId = this.appUser.appUserId;
    appUserGame.attributeId = this.gameTwoAttributeFormControl.value;
    this.gameService.addOrUpdateAttributeForGameAndUser(appUserGame).subscribe(
      (response: AppUserGame) => {
        this.notificationService.notify(NotificationType.INFO, "Atrybut został zmieniony");
      }, (error: HttpErrorResponse) => {
        this.notificationService.notify(NotificationType.ERROR, error.message);
      });
  }
  public setGameThreeAttribute() {
    const appUserGame = new AppUserGame();
    appUserGame.gameId = 3;
    appUserGame.appUserId = this.appUser.appUserId;
    appUserGame.attributeId = this.gameThreeAttributeFormControl.value;
    this.gameService.addOrUpdateAttributeForGameAndUser(appUserGame).subscribe(
      (response: AppUserGame) => {
        this.notificationService.notify(NotificationType.INFO, "Atrybut został zmieniony");
      }, (error: HttpErrorResponse) => {
        this.notificationService.notify(NotificationType.ERROR, error.message);
      });
  }

  private loadGames() {
    this.gameService.getGames().subscribe(
      (response: Game[]) => {
        this.games = response;
        document.getElementById('game-one-card').style.background = 'url('+response[0].gameImageUrl+')';
        this.gameOneName = response[0].gameName;
        this.gameOneDescription = response[0].gameDescription;
        this.gameOneAttributeName = response[0].gameAttributeTitle;
        document.getElementById('game-two-card').style.background = 'url('+response[1].gameImageUrl+')';
        this.gameTwoName = response[1].gameName;
        this.gameTwoDescription = response[1].gameDescription;
        this.gameTwoAttributeName = response[1].gameAttributeTitle;
        document.getElementById('game-three-card').style.background = 'url('+response[2].gameImageUrl+')';
        this.gameThreeName = response[2].gameName;
        this.gameThreeDescription = response[2].gameDescription;
        this.gameThreeAttributeName = response[2].gameAttributeTitle;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGameOne() {
    this.attributeService.getAttributesForGame(1).subscribe(
      (response: Attribute[]) => {
        this.attributesForGameOne = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGameTwo() {
    this.attributeService.getAttributesForGame(2).subscribe(
      (response: Attribute[]) => {
        this.attributesForGameTwo = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGameThree() {
    this.attributeService.getAttributesForGame(3).subscribe(
      (response: Attribute[]) => {
        this.attributesForGameThree = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGameOneAndAppUser() {
      const appUserGame = new AppUserGame();
      appUserGame.gameId = 1;
      appUserGame.appUserId = this.appUser.appUserId;
      this.gameService.getAttributeForGameAndUser(appUserGame).subscribe(
        (response: AppUserGame) => {
          this.gameOneAttributeValue = response.attributeId;
          this.gameOneAttributeFormControl = new FormControl(this.gameOneAttributeValue);
        }, (error: HttpErrorResponse) => {
          console.log(error.message);
        });
  }

  private loadAttributesForGameTwoAndAppUser() {
    const appUserGame = new AppUserGame();
    appUserGame.gameId = 2;
    appUserGame.appUserId = this.appUser.appUserId;
    this.gameService.getAttributeForGameAndUser(appUserGame).subscribe(
      (response: AppUserGame) => {
        this.gameTwoAttributeValue = response.attributeId;
        this.gameTwoAttributeFormControl = new FormControl(this.gameTwoAttributeValue);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGameThreeAndAppUser() {
    const appUserGame = new AppUserGame();
    appUserGame.gameId = 3;
    appUserGame.appUserId = this.appUser.appUserId;
    this.gameService.getAttributeForGameAndUser(appUserGame).subscribe(
      (response: AppUserGame) => {
        this.gameThreeAttributeValue = response.attributeId;
        this.gameThreeAttributeFormControl = new FormControl(this.gameThreeAttributeValue);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  refreshSelect(formControl : FormControl): void {
    formControl.updateValueAndValidity();
  }

  protected readonly GenderEnum = GenderEnum;
}
