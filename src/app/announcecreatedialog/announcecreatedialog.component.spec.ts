import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncecreatedialogComponent } from './announcecreatedialog.component';

describe('AnnouncecreatedialogComponent', () => {
  let component: AnnouncecreatedialogComponent;
  let fixture: ComponentFixture<AnnouncecreatedialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnnouncecreatedialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnnouncecreatedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
