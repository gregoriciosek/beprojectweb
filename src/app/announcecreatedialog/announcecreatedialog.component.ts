import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from "../model/appuser";
import {Game} from "../model/game";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppGroupService} from "../service/appgroup.service";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {AppGroup} from "../model/appgroup";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogDataAnnouncement} from "../model/dialogdataannouncement";
import {Announcement} from "../model/announcement";
import {AnnouncementService} from "../service/announcement.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-announcecreatedialog',
  templateUrl: './announcecreatedialog.component.html',
  styleUrl: './announcecreatedialog.component.css'
})
export class AnnouncecreatedialogComponent implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public activeGame: Game = new Game();
  public announcementNewTitle: string ='';
  public announcementNewDescription: string ='';
  public announcementNewGroupIdFormControl = new FormControl();
  public appGroups: AppGroup[] = new Array<AppGroup>();

  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public announcementService: AnnouncementService,
              public dialogRef: MatDialogRef<AnnouncecreatedialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogDataAnnouncement,
  ) {  }

  ngOnInit(): void {
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
    this.getGroups();
  }

  ngOnDestroy(): void {
  }

  public getGroups() {
    this.appGroupService.getAppGroupsForAppUserForAnnouncements(this.appUser.appUserId, this.gameService.getGameFromLocalCache().gameId).subscribe(
      (response: AppGroup[]) => {
        console.log(response);
          if (response !== null) {
            this.appGroups = response;
          } else {
            this.notificationService.notify(NotificationType.ERROR, "Nie znaleziono grupy, dla której można utworzyć ogłoszenie");
            this.dialogRef.close();
          }
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  createNewAnnoucement(){
    const annoucement: Announcement = new Announcement();
    annoucement.appGroupId=this.announcementNewGroupIdFormControl.value;
    annoucement.appUserId=this.appUser.appUserId;
    annoucement.gameId=this.activeGame.gameId;
    annoucement.announcementTitle=this.announcementNewTitle;
    annoucement.announcementDescription=this.announcementNewDescription;
    this.announcementService.createAnnouncement(annoucement).subscribe(
      (response: Announcement) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Utworzono ogłoszenie " + response.announcementTitle);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }


}
