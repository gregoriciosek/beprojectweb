import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppUser} from "../model/appuser";
import {AuthenticationService} from "../service/authentication.service";
import {GameService} from "../service/game.service";
import {AppuserService} from "../service/appuser.service";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {MessagedialogComponent} from "../messagedialog/messagedialog.component";
import {GenderEnum} from "../model/genderEnum";
import {Attribute} from "../model/attribute";
import {AttributeService} from "../service/attribute.service";
import {FormControl} from "@angular/forms";
import {AppUserFilter} from "../model/appuserfilter";

@Component({
  selector: 'app-appuser',
  templateUrl: './appuser.component.html',
  styleUrl: './appuser.component.css'
})
export class AppuserComponent implements OnInit, OnDestroy{

  public appUser: AppUser;
  public appUsers: AppUser[] = new Array<AppUser>();
  public actualGame: string = '';
  public actualGameAttributeName: string ='';
  public selectedGender: string = '';
  public attributesForGame: Attribute[] = new Array<Attribute>();
  public gameAttributeFormControl = new FormControl();
  public ageFromFormControl = new FormControl();
  public ageToFormControl = new FormControl();
  public genderFormControl = new FormControl();


  constructor(public dialog: MatDialog,
              private router: Router,
              private authenticationService: AuthenticationService,
              private appUserService: AppuserService,
              private attributeService: AttributeService,
              private gameService: GameService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.actualGame = this.gameService.getGameFromLocalCache().gameName;
      this.actualGameAttributeName = this.gameService.getGameFromLocalCache().gameAttributeTitle;
      this.getUsers();
      this.loadAttributesForGame(this.gameService.getGameFromLocalCache().gameId);
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {
  }

  openAppUserDialog(appUserId : number): void {
    const appUserToDialog : AppUser = this.appUsers.find(i => i.appUserId === appUserId);
    const dialogRef = this.dialog.open(AppuserdialogComponent, {
      width: "800px", height: "735px", data: {appUser: appUserToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openMessageDialog(appUserId : number): void {
    const appUserToDialog : AppUser = this.appUsers.find(i => i.appUserId === appUserId);
    const dialogRef = this.dialog.open(MessagedialogComponent, {
      width: "500px", height: "450px", data: {appUser: appUserToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }
  public goToHome() {
    this.router.navigateByUrl('/home');
  }
  public goToChat() {
    this.router.navigateByUrl('/chat');
  }
  public goToProfile() {
    this.router.navigateByUrl('/profile');
  }

  public goToGamerProfile(appUserId : number) {
     this.openAppUserDialog(appUserId);
  }

  public getUsers() {
    this.appUserService.getUsers().subscribe(
      (response: AppUser[]) => {
        this.appUsers = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  private loadAttributesForGame(gameId: number) {
    this.attributeService.getAttributesForGame(gameId).subscribe(
      (response: Attribute[]) => {
        this.attributesForGame = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public filterAppUsers() {
    const appUserFilter: AppUserFilter = new AppUserFilter();
    appUserFilter.gameAttribute = this.gameAttributeFormControl.value;
    appUserFilter.ageFrom = this.ageFromFormControl.value;
    appUserFilter.ageTo = this.ageToFormControl.value;
    appUserFilter.gender = this.genderFormControl.value;
    this.appUserService.getUsersByFilter(appUserFilter).subscribe(
      (response: AppUser[]) => {
        this.appUsers = response;
        console.log(response);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  protected readonly GenderEnum = GenderEnum;
}
