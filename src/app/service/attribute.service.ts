import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import {Attribute} from "../model/attribute";
import {AppUserGame} from "../model/appusergame";

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public getAttributesForGame(gameId: number): Observable<Attribute[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<Attribute[]>(`${this.host}/games/` +  gameId + `/attributes`, {headers: new HttpHeaders(headers)});
  }

  public getAttributeForGameIdAndAppUserId(appUserGame: AppUserGame) : Observable<Attribute> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<Attribute>(`${this.host}/attributes/get`, appUserGame, {headers: new HttpHeaders(headers)});
  }

}
