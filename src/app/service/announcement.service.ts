import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import {Announcement} from "../model/announcement";
import {AppGroupPlayer} from "../model/appgroupplayer";

@Injectable({
  providedIn: 'root'
})
export class AnnouncementService {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public getAnnouncementsByAnnouncementId(announcementId: number) : Observable<Announcement> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<Announcement>(`${this.host}/announcements/` + announcementId, {headers: new HttpHeaders(headers)});
  }

  public getAnnouncementsByGameId(gameId: number) : Observable<Announcement[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<Announcement[]>(`${this.host}/announcements/games/` + gameId, {headers: new HttpHeaders(headers)});
  }

  public createAnnouncement(announcement: Announcement): Observable<Announcement> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<Announcement>(`${this.host}/announcements`, announcement, {headers: new HttpHeaders(headers)});
  }

  public updateAnnouncement(announcement: Announcement): Observable<Announcement> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.put<Announcement>(`${this.host}/announcements`, announcement, {headers: new HttpHeaders(headers)});
  }

  public deleteAnnouncement(announcement: Announcement): Observable<any> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.delete<any>(`${this.host}/announcements`, {headers: new HttpHeaders(headers), body: announcement});
  }

  public addAppUserToGroupByAnnouncement(appGroupPlayer: AppGroupPlayer): Observable<Announcement> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<Announcement>(`${this.host}/announcements/groups/users`, appGroupPlayer, {headers: new HttpHeaders(headers)});
  }

}
