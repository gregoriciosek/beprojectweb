import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import {ChatUsersAndChatMessage} from "../model/chatusersandchatmessage";
import {Chat} from "../model/chat";
import {ChatMessage} from "../model/chatmessage";
import {AppUser} from "../model/appuser";

@Injectable({
  providedIn: 'root'
})
export class Chatservice {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public addOrUpdateChatWithMessage(chatUsersAndChatMessage: ChatUsersAndChatMessage): Observable<Chat> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<Chat>(`${this.host}/chats`, chatUsersAndChatMessage, {headers: new HttpHeaders(headers)});
  }

  public getMessagesByAppUserIds(chatUsersAndChatMessage: ChatUsersAndChatMessage): Observable<ChatMessage[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<ChatMessage[]>(`${this.host}/chats/users/messages`, chatUsersAndChatMessage, {headers: new HttpHeaders(headers)});
  }

  public getAppUsersByChatsByAppUserId(appUserId: number) : Observable<AppUser[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppUser[]>(`${this.host}/chats/users/` + appUserId, {headers: new HttpHeaders(headers)});
  }


}
