import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppUser } from '../model/appuser';
import {Game} from "../model/game";
import {AuthenticationService} from "./authentication.service";
import {AppUserGame} from "../model/appusergame";
import {Attribute} from "../model/attribute";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public getGames(): Observable<Game[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<Game[]>(`${this.host}/games`, {headers: new HttpHeaders(headers)});
  }

  public getAttributeForGameAndUser(appUserGame: AppUserGame): Observable<AppUserGame> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppUserGame>(`${this.host}/games/attributes`, appUserGame, {headers: new HttpHeaders(headers)});
  }

  public addOrUpdateAttributeForGameAndUser(appUserGame: AppUserGame): Observable<AppUserGame> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.put<AppUserGame>(`${this.host}/games/attributes`, appUserGame, {headers: new HttpHeaders(headers)});
  }

  public addGameToLocalCache(game: Game): void {
    sessionStorage.setItem('game', JSON.stringify(game));
  }

  public removeGameFromLocalCache(): void {
    sessionStorage.removeItem('game');
  }

  public getGameFromLocalCache(): Game {
    return JSON.parse(sessionStorage.getItem('game') || '');
  }

}
