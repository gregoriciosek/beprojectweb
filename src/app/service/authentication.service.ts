import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppUser } from '../model/appuser';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NotificationService } from './notification.service';
import { NotificationType } from '../enum/notificationtype.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private host: string = environment.apiUrl;
  private token: any;
  private loggenInUsername: any;
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient,
    private notificationService: NotificationService) {}

  public login(appUser : AppUser): Observable<HttpResponse<AppUser>> {
    return this.http.post<AppUser>(`${this.host}/login`, appUser, {observe: 'response'});
  }

  public register(appUser : AppUser): Observable<HttpResponse<AppUser>> {
    return this.http.post<AppUser>(`${this.host}/register`, appUser, {observe: 'response'});
  }

  public logOut(): void {
    this.logOutWoNotification();
    this.notificationService.notify(NotificationType.INFO, `Wylogowano pomyślnie.`);
  }

  public logOutWoNotification(): void {
    this.token = null;
    this.loggenInUsername = null;
    sessionStorage.removeItem('appUser');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('appUsers');
  }

  public saveToken(token: string): void {
    this.token = token;
    sessionStorage.setItem('token', token);
  }

  public loadToken(): void {
    this.token = sessionStorage.getItem('token');
  }

  public getToken(): string {
    return this.token;
  }

  public addAppUserToLocalCache(appUser: AppUser): void {
    sessionStorage.setItem('appUser', JSON.stringify(appUser));
  }

  public getAppUserFromLocalCache(): AppUser {
    return JSON.parse(sessionStorage.getItem('appUser') || '');
  }

  public isLoggedIn(): boolean {
    this.loadToken();
    if ((this.token != null && this.token !== '')
      && (this.jwtHelper.decodeToken(this.token).sub != null || '')
      && (!this.jwtHelper.isTokenExpired(this.token))) {
          this.loggenInUsername = this.jwtHelper.decodeToken(this.token).sub;
          return true;
    } else {
      this.logOutWoNotification();
      return false;
    }
  }

}
