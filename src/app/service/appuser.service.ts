import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppUser } from '../model/appuser';
import {Game} from "../model/game";
import {AuthenticationService} from "./authentication.service";
import {AppUserFilter} from "../model/appuserfilter";

@Injectable({
  providedIn: 'root'
})
export class AppuserService {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {}

  public getUsers(): Observable<AppUser[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppUser[]>(`${this.host}/users`, {headers: new HttpHeaders(headers)});
  }

  public getUsersByFilter(appUserFilter: AppUserFilter): Observable<AppUser[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppUser[]>(`${this.host}/users/filtered`, appUserFilter,{headers: new HttpHeaders(headers)});
  }

  public addUser(appUser: AppUser): Observable<AppUser | HttpErrorResponse> {
    return this.http.post<AppUser>(`${this.host}/users`, appUser);
  }

  public updateUser(appUser : AppUser): Observable<HttpResponse<AppUser>> {
    return this.http.put<AppUser>(`${this.host}/users`, appUser, {observe: 'response'});
  }

  public deleteUser(appUserId: number): Observable<any | HttpErrorResponse> {
    return this.http.delete<any>(`${this.host}/users/${appUserId}`);
  }

  public addAppUsersTosessionStorage(appUsers: AppUser[]): void {
    sessionStorage.setItem('appUsers', JSON.stringify(appUsers));
  }

  public getAppUsersFromsessionStorage(): AppUser[] | null {
    if (sessionStorage.getItem('appUsers')) {
      return JSON.parse(sessionStorage.getItem('appUsers') || '');
    }
    return null;
  }

  public createUserFromData(appUser : AppUser): FormData {
    const appUserformData = new FormData();

    appUserformData.append('appUserId', JSON.stringify(appUser.appUserId));
    appUserformData.append('appUserTypeId', JSON.stringify(appUser.appUserTypeId));

    appUserformData.append('appUserUsername', appUser.appUserUsername);
    appUserformData.append('appUserPassword', appUser.appUserPassword);
    appUserformData.append('appUserEmail', appUser.appUserEmail);
    appUserformData.append('appUserNickname', appUser.appUserNickname);
    appUserformData.append('appUserDateOfBirth', JSON.stringify(appUser.appUserDateOfBirth));

    appUserformData.append('appUserDiscord', appUser.appUserDiscord);
    appUserformData.append('appUserTeamspeak', appUser.appUserTeamspeak);
    appUserformData.append('appUserGender', appUser.appUserGender);

    return appUserformData;
  }

  public getAppUserByAppUserId(appUserId: number): Observable<AppUser> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppUser>(`${this.host}/users/` + appUserId, {headers: new HttpHeaders(headers)});
  }

}
