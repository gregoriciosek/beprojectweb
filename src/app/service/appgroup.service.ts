import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import {AppUser} from "../model/appuser";
import {AppGroup} from "../model/appgroup";
import {AppGroupChatMessage} from "../model/appgroupchatmessage";

@Injectable({
  providedIn: 'root'
})
export class AppGroupService {

  private host: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public getAppGroupByAppGroupId(appGroupId: number) : Observable<AppGroup> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppGroup>(`${this.host}/groups/` + appGroupId, {headers: new HttpHeaders(headers)});
  }

  public getAppGroupsForAppUser(appUserId: number, gameId: number) : Observable<AppGroup[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppGroup[]>(`${this.host}/groups/users/` + appUserId, gameId,{headers: new HttpHeaders(headers)});
  }

  public getAppGroupsForGameId(gameId: number) : Observable<AppGroup[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppGroup[]>(`${this.host}/groups/games/` + gameId,{headers: new HttpHeaders(headers)});
  }

  public getAppGroupsForAppUserForAnnouncements(appUserId: number, gameId: number) : Observable<AppGroup[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppGroup[]>(`${this.host}/groups/users/` + appUserId +`/noanno`, gameId,{headers: new HttpHeaders(headers)});
  }

  public getAppUsersForAppGroup(appGroupId: number) : Observable<AppUser[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppUser[]>(`${this.host}/groups/` + appGroupId + `/users`, {headers: new HttpHeaders(headers)});
  }

  public getAppGroupChatMessages(appGroupId: number) : Observable<AppGroupChatMessage[]> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.get<AppGroupChatMessage[]>(`${this.host}/groups/` + appGroupId + `/messages`, {headers: new HttpHeaders(headers)});
  }

  public createAppGroup(appGroup: AppGroup): Observable<AppGroup> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppGroup>(`${this.host}/groups`, appGroup, {headers: new HttpHeaders(headers)});
  }

  public updateAppGroup(appGroup: AppGroup): Observable<AppGroup> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.put<AppGroup>(`${this.host}/groups`, appGroup, {headers: new HttpHeaders(headers)});
  }

  public deleteAppGroup(appGroupId: number): Observable<any> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.delete<any>(`${this.host}/groups/` + appGroupId, {headers: new HttpHeaders(headers)});
  }

  public addAppUserToAppGroup(appGroupId: number, appUserId: number): Observable<AppGroup> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppGroup>(`${this.host}/groups/` + appGroupId + `/users`, appUserId, {headers: new HttpHeaders(headers)});
  }

  public removeAppUserFromAppGroup(appGroupId: number, appUserId: number): Observable<AppGroup> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.delete<AppGroup>(`${this.host}/groups/` + appGroupId + `/users`, {headers: new HttpHeaders(headers), body: appUserId});
  }

  public addAppGroupChatMessageToAppGroup(appGroupId: number, appGroupChatMessage: AppGroupChatMessage) : Observable<AppGroupChatMessage> {
    const headers = {
      'Authorization': `${this.authenticationService.getToken()}`,
      'Content-Type':'application/json'
    };
    return this.http.post<AppGroupChatMessage>(`${this.host}/groups/` + appGroupId + `/messages`, appGroupChatMessage,{headers: new HttpHeaders(headers)});
  }

}
