import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from "../model/appuser";
import {Game} from "../model/game";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppGroupService} from "../service/appgroup.service";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {AnnouncementService} from "../service/announcement.service";
import {DialogDataAnnouncement} from "../model/dialogdataannouncement";
import {Announcement} from "../model/announcement";

@Component({
  selector: 'app-announceeditdialog',
  templateUrl: './announceeditdialog.component.html',
  styleUrl: './announceeditdialog.component.css'
})
export class AnnounceeditdialogComponent implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public activeGame: Game = new Game();
  public announcementUpdateTitle: string ='';
  public announcementUpdateDescription: string ='';
  public announcementGroupName: string = '';
  public announcement: Announcement = new Announcement();

  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public announcementService: AnnouncementService,
              public dialogRef: MatDialogRef<AppuserdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogDataAnnouncement,
  ) {  }

  ngOnInit(): void {
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
    this.announcement = this.data.announcement;
    this.announcementUpdateTitle = this.data.announcement.announcementTitle;
    this.announcementUpdateDescription = this.data.announcement.announcementDescription;
    this.announcementGroupName = this.data.appGroup.appGroupName;

  }

  ngOnDestroy(): void {
  }

  updateNewAnnoucement(){
    const annoucement: Announcement = this.announcement;
    annoucement.announcementTitle = this.announcementUpdateTitle;
    annoucement.announcementDescription = this.announcementUpdateDescription;
    this.announcementService.updateAnnouncement(annoucement).subscribe(
      (response: Announcement) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Zaaktualizowano ogłoszenie " + response.announcementTitle);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }


}
