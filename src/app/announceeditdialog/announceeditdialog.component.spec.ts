import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnounceeditdialogComponent } from './announceeditdialog.component';

describe('AnnounceeditdialogComponent', () => {
  let component: AnnounceeditdialogComponent;
  let fixture: ComponentFixture<AnnounceeditdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnnounceeditdialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnnounceeditdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
