import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncedialogComponent } from './announcedialog.component';

describe('AnnouncedialogComponent', () => {
  let component: AnnouncedialogComponent;
  let fixture: ComponentFixture<AnnouncedialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnnouncedialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnnouncedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
