import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from "../model/appuser";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {Game} from "../model/game";
import {Announcement} from "../model/announcement";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppGroupService} from "../service/appgroup.service";
import {DialogDataAnnouncement} from "../model/dialogdataannouncement";
import {AppGroup} from "../model/appgroup";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {AppuserService} from "../service/appuser.service";
import {NotificationType} from "../enum/notificationtype.enum";

@Component({
  selector: 'app-announcedialog',
  templateUrl: './announcedialog.component.html',
  styleUrl: './announcedialog.component.css'
})
export class AnnouncedialogComponent  implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public announcementAppUser: AppUser = new AppUser();
  public appUsers: AppUser[] = new Array<AppUser>();
  public activeGame: Game = new Game();
  public announcement: Announcement = new Announcement();
  public appGroup: AppGroup = new AppGroup();
  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public appUserService: AppuserService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public dialogRef: MatDialogRef<AnnouncedialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogDataAnnouncement,
  ) {  }

  ngOnInit(): void {
    this.announcement = this.data.announcement;
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
    this.appGroup = this.data.appGroup;
    this.getUsers();
    console.log(this.announcementAppUser);
  }
  ngOnDestroy(): void {}

  addAppUserToGroup() {
    this.appGroupService.addAppUserToAppGroup(this.appGroup.appGroupId, this.appUser.appUserId).subscribe(
      (response: AppGroup) => {
        this.notificationService.notify(NotificationType.SUCCESS,"Dołączyłeś do grupy "+ this.appGroup.appGroupName);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  openAppUserDialog(appUserId : number): void {
    const appUserToDialog : AppUser = this.appUsers.find(i => i.appUserId === appUserId);
    const dialogRef = this.dialog.open(AppuserdialogComponent, {
      width: "800px", height: "735px", data: {appUser: appUserToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public getUsers() {
    this.appUserService.getUsers().subscribe(
      (response: AppUser[]) => {
        this.appUsers = response;
        this.announcementAppUser = this.appUsers.find(value => value.appUserId === this.announcement.appUserId);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }
}
