import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AppUser} from "../model/appuser";
import {Router} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {GameService} from "../service/game.service";
import {ArrayType} from "@angular/compiler";
import {Chatservice} from "../service/chatservice";
import {AppUserGame} from "../model/appusergame";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {ChatUsersAndChatMessage} from "../model/chatusersandchatmessage";
import {ChatMessage} from "../model/chatmessage";
import {MatList} from "@angular/material/list";
import {Chat} from "../model/chat";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrl: './chat.component.css'
})
export class ChatComponent implements OnInit, OnDestroy {

  @ViewChild('messageList') messageList: MatList;

  public appUser: AppUser = new AppUser();
  public actualChatUser = new AppUser();
  public chatUsers: AppUser[] = new Array<AppUser>();
  public chatMessages: ChatMessage[] = new Array<ChatMessage>();
  public dataRefreshed = true;
  public message: string ='';

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private gameService: GameService,
              private chatService: Chatservice) {
  }

  ngOnInit(): void {
    if (this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.getChatUsers(this.appUser.appUserId);
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {

  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }

  public goToProfile() {
    this.router.navigateByUrl('/profile');
  }

  public goToHome() {
    this.router.navigateByUrl('/home');
  }

  public getChatUsers(appUserId: number) {
    this.chatService.getAppUsersByChatsByAppUserId(appUserId).subscribe(
      (response: AppUser[]) => {
        this.chatUsers = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public loadMessagesForUser(appUserId: number) {
    this.actualChatUser = this.chatUsers.find(value => value.appUserId === appUserId);
    const chatUsersAndChatMessage: ChatUsersAndChatMessage
      = new ChatUsersAndChatMessage();
    chatUsersAndChatMessage.appUserId = this.appUser.appUserId;
    chatUsersAndChatMessage.appAppUserId = appUserId;
    this.chatService.getMessagesByAppUserIds(chatUsersAndChatMessage).subscribe(
      (response: ChatMessage[]) => {
      this.chatMessages = response;
      this.dataRefreshed = true;
      console.log(this.chatMessages.at(0).chatMessageMsg);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public sendMessage() {
    const chatUsersAndChatMessage: ChatUsersAndChatMessage = new ChatUsersAndChatMessage();
    chatUsersAndChatMessage.appUserId = this.authenticationService.getAppUserFromLocalCache().appUserId;
    chatUsersAndChatMessage.appAppUserId = this.actualChatUser.appUserId;
    chatUsersAndChatMessage.chatMessageMsg = this.message;
    this.chatService.addOrUpdateChatWithMessage(chatUsersAndChatMessage).subscribe(
      (response: Chat) => {
        this.loadMessagesForUser(this.actualChatUser.appUserId);
        this.message ='';
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public getFormattedDate(date: Date) : string {
    const dateFromString = new Date();
    dateFromString.setTime(Date.parse(date.toString()));
     return (dateFromString.getFullYear() + '-' +
        (dateFromString.getMonth()+1).toString().padStart(2,'0') + '-' +
        dateFromString.getDate().toString().padStart(2,'0') + ' :: ' +
        dateFromString.getHours().toString().padStart(2,'0') + ':' +
        dateFromString.getMinutes().toString().padStart(2,'0') + ':' +
        dateFromString.getSeconds().toString().padStart(2,'0'));
  }

}
