import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Game} from "../model/game";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {GenderEnum} from "../model/genderEnum";
import {AppGroup} from "../model/appgroup";
import {DialogDataAppGroup} from "../model/dialogdataappgroup";
import {AppUser} from "../model/appuser";
import {AppGroupService} from "../service/appgroup.service";
import {HttpErrorResponse} from "@angular/common/http";
import {AppGroupChatMessage} from "../model/appgroupchatmessage";
import {AuthenticationService} from "../service/authentication.service";

@Component({
  selector: 'app-appgroupdialog',
  templateUrl: './appgroupdialog.component.html',
  styleUrl: './appgroupdialog.component.css'
})
export class AppgroupdialogComponent implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public appGroupToDialog: AppGroup = new AppGroup();
  public activeGame: Game = new Game();
  public appUsersInGroup: AppUser[] = new Array<AppUser>();
  public appGroupChatMessagesInGroup: AppGroupChatMessage[] = new Array<AppGroupChatMessage>();
  public messageToBeSend: string ='';
  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public authenticationService: AuthenticationService,
              public dialogRef: MatDialogRef<AppuserdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogDataAppGroup,
  ) {  }

  ngOnInit(): void {
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
    this.appGroupToDialog = this.data.appGroup;
    this.loadAppUsersInGroup(this.data.appGroup.appGroupId);
    this.loadAppGroupChatMessagesInGroup(this.data.appGroup.appGroupId);
  }

  ngOnDestroy(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public loadAppUsersInGroup(appGroupId: number) {
    this.appGroupService.getAppUsersForAppGroup(appGroupId).subscribe(
      (response: AppUser[]) => {
        this.appUsersInGroup = response;
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public loadAppGroupChatMessagesInGroup(appGroupId: number) {
    console.log(appGroupId);
    this.appGroupService.getAppGroupChatMessages(appGroupId).subscribe(
      (response: AppGroupChatMessage[]) => {
        this.appGroupChatMessagesInGroup = response;
        console.log(response);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  public getAppUserFromAppUsersInGroup(appUserId: number): AppUser {
    return this.appUsersInGroup.find(value => value.appUserId === appUserId);
  }
  public getFormattedDate(date: Date) : string {
    const dateFromString = new Date();
    dateFromString.setTime(Date.parse(date.toString()));
    return (dateFromString.getFullYear() + '-' +
      (dateFromString.getMonth()+1).toString().padStart(2,'0') + '-' +
      dateFromString.getDate().toString().padStart(2,'0') + ' :: ' +
      dateFromString.getHours().toString().padStart(2,'0') + ':' +
      dateFromString.getMinutes().toString().padStart(2,'0') + ':' +
      dateFromString.getSeconds().toString().padStart(2,'0'));
  }

  public sendMessage() {
    const appGroupChatMessage: AppGroupChatMessage = new AppGroupChatMessage();
    appGroupChatMessage.appUserId = this.authenticationService.getAppUserFromLocalCache().appUserId;
    appGroupChatMessage.appGroupChatMessageMsg = this.messageToBeSend;
    this.appGroupService.addAppGroupChatMessageToAppGroup(this.appGroupToDialog.appGroupId, appGroupChatMessage).subscribe(
      (response: AppGroupChatMessage) => {
        this.loadAppGroupChatMessagesInGroup(this.appGroupToDialog.appGroupId);
        this.messageToBeSend='';
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  protected readonly GenderEnum = GenderEnum;
}
