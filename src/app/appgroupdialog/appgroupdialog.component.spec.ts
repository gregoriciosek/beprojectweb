import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppgroupdialogComponent } from './appgroupdialog.component';

describe('AppgroupdialogComponent', () => {
  let component: AppgroupdialogComponent;
  let fixture: ComponentFixture<AppgroupdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppgroupdialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AppgroupdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
