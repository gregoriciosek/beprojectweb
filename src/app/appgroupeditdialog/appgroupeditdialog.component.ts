import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from "../model/appuser";
import {Game} from "../model/game";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {GameService} from "../service/game.service";
import {AppGroupService} from "../service/appgroup.service";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {AppuserdialogComponent} from "../appuserdialog/appuserdialog.component";
import {DialogDataAppGroup} from "../model/dialogdataappgroup";
import {AppGroup} from "../model/appgroup";
import {NotificationType} from "../enum/notificationtype.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {GenderEnum} from "../model/genderEnum";

@Component({
  selector: 'app-appgroupeditdialog',
  templateUrl: './appgroupeditdialog.component.html',
  styleUrl: './appgroupeditdialog.component.css'
})
export class AppgroupeditdialogComponent implements OnInit, OnDestroy{

  public appUser:AppUser = new AppUser();
  public activeGame: Game = new Game();
  public appGroupToDialog: AppGroup = new AppGroup();
  public appGroupUpdateName: string ='';
  public appGroupUpdateDescription: string ='';
  constructor(public dialog: MatDialog,
              public gameService: GameService,
              public appGroupService: AppGroupService,
              public authenticationService: AuthenticationService,
              public notificationService: NotificationService,
              public dialogRef: MatDialogRef<AppuserdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogDataAppGroup,
  ) {  }

  ngOnInit(): void {
    this.appUser = this.authenticationService.getAppUserFromLocalCache();
    this.activeGame = this.gameService.getGameFromLocalCache();
    this.appGroupToDialog = this.data.appGroup;
    this.appGroupUpdateName = this.appGroupToDialog.appGroupName;
    this.appGroupUpdateDescription = this.appGroupToDialog.appGroupDescription;
  }

  ngOnDestroy(): void {
  }

  updateGroup(){
    const appGroup: AppGroup = this.appGroupToDialog;
    appGroup.appGroupName = this.appGroupUpdateName;
    appGroup.appGroupDescription = this.appGroupUpdateDescription;
    this.appGroupService.updateAppGroup(appGroup).subscribe(
      (response: AppGroup) => {
        this.notificationService.notify(NotificationType.SUCCESS, "Edytowano grupę " + appGroup.appGroupName);
        this.dialogRef.close();
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      });
  }

  protected readonly GenderEnum = GenderEnum;
}
