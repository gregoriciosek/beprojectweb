import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppgroupeditdialogComponent } from './appgroupeditdialog.component';

describe('AppgroupeditdialogComponent', () => {
  let component: AppgroupeditdialogComponent;
  let fixture: ComponentFixture<AppgroupeditdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppgroupeditdialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AppgroupeditdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
