import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppuserComponent } from './appuser/appuser.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import {GameComponent} from "./game/game.component";
import {AnnouncementComponent} from "./announcement/announcement.component";
import {AppGroupComponent} from "./appgroup/appgroup.component";
import {ChatComponent} from "./chat/chat.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'player', component: AppuserComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'game', component: GameComponent},
  {path: 'announcement', component: AnnouncementComponent},
  {path: 'group', component: AppGroupComponent},
  {path: 'chat', component: ChatComponent},
  {path: '**', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
