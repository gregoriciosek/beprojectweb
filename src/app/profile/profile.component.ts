import {Component, OnDestroy, OnInit, signal} from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { AppUser } from '../model/appuser';
import { FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from '../utils/my-error-state-matcher';
import {AppuserService} from "../service/appuser.service";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {NotificationType} from "../enum/notificationtype.enum";
import {NotificationService} from "../service/notification.service";
import {GenderEnum} from "../model/genderEnum";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.css'
})
export class ProfileComponent implements OnInit, OnDestroy{

  public appUser: AppUser;
  public pictureUrlControl : FormControl;
  public emailFormControl : FormControl;
  public matcher = new MyErrorStateMatcher();
  public nicknameFormControl : FormControl;
  public dateOfBirthFormControl : FormControl;
  public discordUrlFormControl : FormControl;
  public teamspeakUrlFormControl : FormControl;
  public genderFormControl: FormControl;

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
              private appUserService: AppuserService,
              private notificationService: NotificationService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      this.appUser = this.authenticationService.getAppUserFromLocalCache();
      this.nicknameFormControl = new FormControl(this.appUser.appUserNickname);
      this.pictureUrlControl = new FormControl(this.appUser.appUserProfileImgUrl);
      this.emailFormControl = new FormControl(this.appUser.appUserEmail, [Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);
      if (this.appUser.appUserDateOfBirth != null) {
        const dateFromString = new Date();
        dateFromString.setTime(Date.parse(this.appUser.appUserDateOfBirth.toString()));
        this.dateOfBirthFormControl = new FormControl(
          (dateFromString.getFullYear() + '-' +
            (dateFromString.getMonth()+1).toString().padStart(2,'0') + '-' +
            dateFromString.getDate().toString().padStart(2,'0')));
      }
      this.genderFormControl = new FormControl(this.appUser.appUserGender);
      this.discordUrlFormControl = new FormControl(this.appUser.appUserDiscord);
      this.teamspeakUrlFormControl = new FormControl(this.appUser.appUserTeamspeak);
      console.log(this.appUser);
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {}

  goToHome() {
    this.router.navigateByUrl("/home");
  }

  goToChat() {
    this.router.navigateByUrl("/chat");
  }

  public logOut() {
    this.authenticationService.logOut();
    this.router.navigateByUrl('/login');
  }

  changePictureUrl() {
    this.appUser.appUserProfileImgUrl = this.pictureUrlControl.value;
    this.appUserService.updateUser(this.appUser).subscribe(
      (response: HttpResponse<AppUser>) => {
        this.notificationService.notify(NotificationType.INFO, "Zaaktualizowano zdjęcie profilowe.");
        this.authenticationService.addAppUserToLocalCache(this.appUser);
      }, (error: HttpErrorResponse) => {
        this.notificationService.notify(NotificationType.ERROR, error.message);
      });
  }

  changeProfileData() {
    this.appUser.appUserEmail = this.emailFormControl.value;
    this.appUser.appUserNickname = this.nicknameFormControl.value;
    if(this.dateOfBirthFormControl.value != null) {
      this.appUser.appUserDateOfBirth = this.dateOfBirthFormControl.value;
    }
    this.appUser.appUserGender = this.genderFormControl.value;
    this.appUser.appUserDiscord = this.discordUrlFormControl.value;
    this.appUser.appUserTeamspeak = this.teamspeakUrlFormControl.value;
    this.appUserService.updateUser(this.appUser).subscribe(
      (response: HttpResponse<AppUser>) => {
        this.notificationService.notify(NotificationType.INFO, "Zaaktualizowano dane konta.");
        this.authenticationService.addAppUserToLocalCache(this.appUser);
      }, (error: HttpErrorResponse) => {
        this.notificationService.notify(NotificationType.ERROR, error.message);
      });
    console.log(this.appUser);
  }

  protected readonly GenderEnum = GenderEnum;
}
