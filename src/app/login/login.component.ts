import { Component, OnDestroy, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { AppUser } from '../model/appuser';
import { Subscription } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { NotificationService } from '../service/notification.service';
import { NotificationType } from '../enum/notificationtype.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit, OnDestroy{

  public showLoading: boolean = false;
  private subscription: Subscription[] = [];

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService) {}

  ngOnInit(): void {
    if(this.authenticationService.isLoggedIn()) {
      console.log('loggedIn');
      this.router.navigateByUrl('/game');
    } else {
      console.log('notLoggedIn');
      this.router.navigateByUrl('/login');
    }
  }

  public onLogin(appUser: AppUser): void {
    this.showLoading = true;
    this.subscription.push(
      this.authenticationService.login(appUser).subscribe(
        (response: HttpResponse<AppUser>) => {
          this.notificationService.notify(NotificationType.INFO, `Pomyślnie zalogowano do systemu.`);
          const token = response.headers.get('Jwt-Token');
          this.authenticationService.saveToken(token || '');
          console.log(response);
          this.authenticationService.addAppUserToLocalCache(response.body);
          this.notificationService.notify(NotificationType.SUCCESS, `Witaj na serwerze ` + response.body.appUserUsername + `!`);
          this.router.navigateByUrl('/game');
          this.showLoading = false;
        }, (error: HttpErrorResponse) => {
          console.log(error);
          this.notificationService.notify(NotificationType.ERROR, error.error.message);
          this.showLoading = false;
        })
      );
  }

  ngOnDestroy(): void {

  }
}
