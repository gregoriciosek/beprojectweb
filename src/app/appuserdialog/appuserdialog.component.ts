import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {CdkTextareaAutosize, TextFieldModule} from '@angular/cdk/text-field';
import {AppUser} from "../model/appuser";
import {DialogDataAppUser} from "../model/dialogdataappuser";
import {MessagedialogComponent} from "../messagedialog/messagedialog.component";
import {GenderEnum} from "../model/genderEnum";
import {Game} from "../model/game";
import {AppUserGame} from "../model/appusergame";
import {FormControl} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {GameService} from "../service/game.service";
import {NotificationType} from "../enum/notificationtype.enum";
import {Attribute} from "../model/attribute";
import {AttributeService} from "../service/attribute.service";

@Component({
  selector: 'app-appuserdialog',
  templateUrl: './appuserdialog.component.html',
  styleUrl: './appuserdialog.component.css'
})
export class AppuserdialogComponent implements OnInit, OnDestroy{

  public appUserToDialog : AppUser = new AppUser();
  public experienceInGame: string = '';
  public activeGame: Game = new Game();
  constructor(public dialog: MatDialog,
    public gameService: GameService,
    public attributeService: AttributeService,
    public dialogRef: MatDialogRef<AppuserdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataAppUser,
  ) {  }

  openMessageDialog(): void {
    const dialogRef = this.dialog.open(MessagedialogComponent, {
      width: "500px", height: "440px", data: {appUser: this.appUserToDialog}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit(): void {
    this.activeGame = this.gameService.getGameFromLocalCache();
    const appUserGame: AppUserGame = new AppUserGame();
    appUserGame.appUserId = this.data.appUser.appUserId;
    appUserGame.gameId = this.activeGame.gameId;
    console.log(appUserGame);
      this.attributeService.getAttributeForGameIdAndAppUserId(appUserGame).subscribe(
        (response: Attribute) => {
          if(response != null) {
            this.experienceInGame = response.attributeName;
          } else {
            this.experienceInGame = 'Nie podano';
          }
        }, (error: HttpErrorResponse) => {
          console.log(error.message);
        });
    this.appUserToDialog = this.data.appUser;
  }

  ngOnDestroy(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  protected readonly GenderEnum = GenderEnum;
}
